//
//  AwesomeManager.swift
//  MyAwesomeFramework
//
//  Created by Enrique Melgarejo on 7/8/16.
//  Copyright © 2016 Isobar Brazil. All rights reserved.
//

import Foundation

public struct AwesomeManager {
    
    public static func initManager() {
        print("Awesome Manager init!")
    }
}