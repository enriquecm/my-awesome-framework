//
//  MyAwesomeFramework.h
//  MyAwesomeFramework
//
//  Created by Enrique Melgarejo on 7/8/16.
//  Copyright © 2016 Isobar Brazil. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MyAwesomeFramework.
FOUNDATION_EXPORT double MyAwesomeFrameworkVersionNumber;

//! Project version string for MyAwesomeFramework.
FOUNDATION_EXPORT const unsigned char MyAwesomeFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MyAwesomeFramework/PublicHeader.h>


